Playlist Challenge
==========================

> Build a single page web application for creating playlists from a library of
> songs!

To install and start the web server (Tested with node v12.13, npm v6.12):

```bash
git clone https://gitlab.com/sachin.panday/samsung-playlist.git
cd <application folder>
npm install
npm install pm2 -g
npm run build
cp -r src/data dist/data
cd dist/
Pm2 start index.js
```

