import {
  Arg,
  FieldResolver,
  Mutation,
  Query,
  Resolver,
  Root,
  Int,
} from "type-graphql";
import { PlayListData, LibraryData } from "../data";
import PlayList from "../schemas/PlayList";
import { PlayListService } from "../service/playlist.service";
import { LibraryService } from "../service/library.service"
import PlayListInputType from "../inputType/PlayListInputType";
import "reflect-metadata";
import {container} from "tsyringe";

@Resolver(of => PlayList)
export default class {
  public playListService: PlayListService = container.resolve(PlayListService);
  public libraryService: LibraryService = container.resolve(LibraryService);

  /**
   * fetchPlayList return all playlist available
   */
  @Query(returns => [PlayList], { nullable: true })
  fetchPlayList(): PlayListData[] | undefined {
    const response = this.playListService.getPlayList();
    return response;
  }

  /**
   * getPlayList based on playlist Id
   * @param id
   */
  @Query(returns => PlayList, { nullable: true })
  getPlayList(@Arg("id") id: number): PlayListData | undefined {
    return this.playListService.getPlayList().find(playlist => playlist.id === id);
  }

  /**
   * add new playlist
   * name and songs required
   */
  @Mutation(returns => [PlayList], { nullable: true })
  playListAdd(@Arg("req") req: PlayListInputType): PlayListData[] | undefined {
    if(!req.name){
      throw new SyntaxError("PlayList Name required");
    }
    return this.playListService.createPlayList(req.name, req.songs);
  }

  /**
   * add songs in playList
   * id and songs required
   */
  @Mutation(returns => [PlayList], { nullable: true })
  playListUpdateSongs(@Arg("req") req: PlayListInputType): PlayListData[] | undefined {
    if(!req.id){
      throw new SyntaxError("PlayList id required");
    }
    return this.playListService.updatePlayList(req.id, req.songs);
  }

  @FieldResolver()
  albums(@Root() playlistData: PlayListData) {
    return this.libraryService.getLibrarys().filter(library => {
      return playlistData.songs.indexOf(library.id) > -1;
    });
  }

  /**
  * delete playList from record
  * id required
  */
  @Mutation(returns => [PlayList], { nullable: true })
  playListRemove(@Arg("id") id: number): PlayListData[] | undefined {
    if(!id){
      throw new SyntaxError("PlayList id required");
    }
    return this.playListService.deletePlayList(id);
  }
}
